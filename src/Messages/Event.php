<?php

namespace SmartOver\RabbitMQ\Messages;

use PhpAmqpLib\Message\AMQPMessage;
use SmartOver\RabbitMQ\MessageSender;

/**
 * Class Event
 *
 * @package SmartOver\RabbitMQ\Messages
 */
class Event implements MessageInterface
{
    /**
     * @var string
     */
    private $channel = 'events';

    /**
     * @var string
     */
    private $event;

    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $ip;

    /**
     * @var string
     */
    private $screen;

    /**
     * @var
     */
    private $recordId;

    /**
     * @var array
     */
    private $data;


    public function __construct($event, $userId, $ip, $screen, $recordId, $data)
    {
        $this->event = $event;
        $this->userId = $userId;
        $this->ip = $ip;
        $this->screen = $screen;
        $this->recordId = $recordId;
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return json_encode([
            'event' => $this->event,
            'userId' => $this->userId,
            'ip' => $this->ip,
            'screen' => $this->screen,
            'recordId' => $this->recordId,
            'data' => $this->data,
        ]);
    }

    /**
     * @param \SmartOver\RabbitMQ\MessageSender $sender
     * @return \SmartOver\RabbitMQ\MessageSender
     */
    public function publish(MessageSender $sender): MessageSender
    {

        $sender->channel->queue_declare($this->channel, false, true, false, false);

        $msg = new AMQPMessage($this->getMessage(), ['delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]);
        $sender->channel->basic_publish($msg, '', $this->channel);

        return $sender;
    }
}