<?php

namespace SmartOver\RabbitMQ\Messages;

use SmartOver\RabbitMQ\MessageSender;

/**
 * Interface MessageInterface
 *
 * @package SmartOver\RabbitMQ\Messages
 */
interface MessageInterface
{
    /**
     * @return string
     */
    public function getMessage(): string;

    public function publish(MessageSender $sender): MessageSender;
}